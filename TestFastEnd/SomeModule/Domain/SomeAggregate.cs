using TestFastEnd.Common.Ddd;

namespace TestFastEnd.SomeModule.Domain;

public sealed class SomeAggregate : Entity<Guid>, AggregateRoot<Guid>
{
    public string Name { get; } = null!;
    
    [Obsolete("Only for EF Core", true)]
    private SomeAggregate() {}

    public SomeAggregate(string name)
    {
        Name = name;
        Id = Guid.NewGuid();
    }
}