namespace TestFastEnd.SomeModule.Infrastructure.Api.Dtos;

public record SomeDto(string Name);