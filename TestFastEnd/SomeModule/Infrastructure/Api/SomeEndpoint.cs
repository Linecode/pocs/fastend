using FastEndpoints;
using MediatR;
using TestFastEnd.SomeModule.Application;
using TestFastEnd.SomeModule.Infrastructure.Api.Dtos;
using TestFastEnd.SomeModule.Infrastructure.Api.Resources;

namespace TestFastEnd.SomeModule.Infrastructure.Api;

public class SomeEndpoint : Endpoint<SomeResource, SomeDto>
{
    private readonly ISender _sender;

    public SomeEndpoint(ISender sender)
    {
        _sender = sender;
    }

    public override async Task HandleAsync(SomeResource resource, CancellationToken ct)
    {
        var result = await _sender.Send(new CreateSmth(resource.Name), ct);

        await result.Match(
            async some => await SendAsync(new SomeDto(resource.Name), 201, ct),
            async none => await SendAsync(null, 422, ct)
        );
    }
    
    public override void Configure()
    {
        Post("/some-endpoint");
        AllowAnonymous();
        Description(b => 
            b
                .Accepts<SomeResource>("application/json")
                .Produces<SomeDto>(201, "application/json")
                .ProducesValidationProblem(422));
        Summary(new SomeEndpointSummary());
    }

    private class SomeEndpointSummary : Summary<SomeEndpoint>
    {
        public SomeEndpointSummary()
        {
            Summary = "Some summary";
            Description = "Some description";
            ExampleRequest = new SomeResource("Kamil");
            Response<SomeDto>(200, "oki doki", contentType: "application/json", example: new SomeDto("Kamil"));
            Response(422, "validation problem", contentType: "application/problem+json");
        }
    }
}