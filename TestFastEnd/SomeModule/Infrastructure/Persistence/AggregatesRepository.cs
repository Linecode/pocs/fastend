using TestFastEnd.SomeModule.Domain;
using TestFastEnd.SomeModule.Domain.Repositories;

namespace TestFastEnd.SomeModule.Infrastructure.Persistence;

public class AggregatesRepository : IAggregateRepository
{
    private readonly SomeDbContext _someDbContext;

    public AggregatesRepository(SomeDbContext someDbContext)
    {
        _someDbContext = someDbContext;
    }

    public async Task Add(SomeAggregate aggregate)
    {
        _someDbContext.Add(aggregate);
        
        await _someDbContext.SaveEntitiesAsync();
    }
}