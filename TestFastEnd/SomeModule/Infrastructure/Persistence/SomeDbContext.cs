using Microsoft.EntityFrameworkCore;
using TestFastEnd.Common.EfCore;
using TestFastEnd.SomeModule.Domain;

namespace TestFastEnd.SomeModule.Infrastructure.Persistence;

public class SomeDbContext : DbContext, IUnitOfWork
{
    public DbSet<SomeAggregate> Aggregates { get; private set; } = null!;

    public SomeDbContext(DbContextOptions<SomeDbContext> options) : base(options)
    {
        
    }
    
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(SomeDbContext).Assembly);
        
        base.OnModelCreating(modelBuilder);
    }
}