using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestFastEnd.SomeModule.Domain;

namespace TestFastEnd.SomeModule.Infrastructure.Persistence.Configurations;

public class SomeAggregateEntityTypeConfiguration : IEntityTypeConfiguration<SomeAggregate>
{
    public void Configure(EntityTypeBuilder<SomeAggregate> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Name);

        builder.ToTable("Aggregates");
    }
}