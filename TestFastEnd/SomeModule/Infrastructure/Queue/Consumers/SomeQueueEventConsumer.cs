using System.Text.Json;
using MassTransit;
using MediatR;
using TestFastEndContract;

namespace TestFastEnd.SomeModule.Infrastructure.Queue.Consumers;

public class SomeQueueEventConsumer : IConsumer<SomeQueueEvent>
{
    private readonly ILogger<SomeQueueEventConsumer> _logger;
    private readonly IPublisher _publisher;

    public SomeQueueEventConsumer(ILogger<SomeQueueEventConsumer> logger, IPublisher publisher)
    {
        _logger = logger;
        _publisher = publisher;
    }
    
    public async Task Consume(ConsumeContext<SomeQueueEvent> context)
    {
        _logger.LogInformation("SomeQueueEventConsumer: {SomeQueueEvent}", JsonSerializer.Serialize(context.Message));

        // This is not transactional
        await _publisher.Publish(context.Message);
    }
}