using System.Text.Json;
using MassTransit;
using TestFastEnd.SomeModule.Application.Ports;

namespace TestFastEnd.SomeModule.Infrastructure.Queue;

public class MessagePublisher : IMessagePublisher
{
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly ILogger<MessagePublisher> _logger;

    public MessagePublisher(IPublishEndpoint publishEndpoint, ILogger<MessagePublisher> logger)
    {
        _publishEndpoint = publishEndpoint;
        _logger = logger;
    }

    public async Task Publish<T>(T obj, CancellationToken ct = default) where T : class
    {
        _logger.LogInformation("Publishing {Obj}", JsonSerializer.Serialize(obj));
        await _publishEndpoint.Publish(obj, ct);
    }
}