using MediatR;

// ReSharper disable once CheckNamespace
namespace TestFastEndContract;

// ReSharper disable once ClassNeverInstantiated.Global
public record SomeQueueEvent(Guid Id) : INotification;