using MediatR;
using TestFastEnd.Common;
using TestFastEnd.SomeModule.Application.Ports;
using TestFastEnd.SomeModule.Domain;
using TestFastEnd.SomeModule.Domain.Repositories;
using TestFastEndContract;

namespace TestFastEnd.SomeModule.Application;

public record CreateSmth(string Name) : IRequest<Option<Guid>>
{
    private class CreateSmthHandler : IRequestHandler<CreateSmth, Option<Guid>>
    {
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IMessagePublisher _messagePublisher;

        public CreateSmthHandler(IAggregateRepository aggregateRepository, IMessagePublisher messagePublisher)
        {
            _aggregateRepository = aggregateRepository;
            _messagePublisher = messagePublisher;
        }
        
        public async Task<Option<Guid>> Handle(CreateSmth request, CancellationToken cancellationToken)
        {
            var aggregate = new SomeAggregate(request.Name);

            try
            {
                await _aggregateRepository.Add(aggregate);
                
                // this is not transactional
                await _messagePublisher.Publish(new SomeQueueEvent(aggregate.Id), cancellationToken);
                
                return new Option<Guid>.Some(aggregate.Id);
            }
            catch (Exception ex)
            {
                return new Option<Guid>.None();
            }
        }
    }
}
