using TestFastEnd.SomeModule.Application.Ports;
using TestFastEnd.SomeModule.Domain.Repositories;
using TestFastEnd.SomeModule.Infrastructure.Persistence;
using TestFastEnd.SomeModule.Infrastructure.Queue;

namespace TestFastEnd.SomeModule;

public abstract class SomeModule
{
}

public static class SomeModuleExtensions 
{
    public static IServiceCollection RegisterSomeModuleServices(this IServiceCollection services)
    {
        services.AddScoped<IAggregateRepository, AggregatesRepository>();
        services.AddScoped<IMessagePublisher, MessagePublisher>();

        return services;
    }
}