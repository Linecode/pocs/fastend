using System.Reflection;
using System.Text.Json;
using FastEndpoints;
using FastEndpoints.Swagger;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Serilog;
using TestFastEnd.SomeModule;
using TestFastEnd.SomeModule.Infrastructure.Persistence;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((host, log) =>
{
    if (host.HostingEnvironment.IsProduction())
        log.MinimumLevel.Information();

    log.WriteTo.Console();
});

// This should be in module
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<SomeDbContext>(options => options.UseSqlite(connectionString));
    
builder.Services.AddMediatR(cfg =>
{
    cfg.RegisterServicesFromAssemblyContaining<SomeModule>();
});
builder.Services.AddFastEndpoints(o =>
{
    o.IncludeAbstractValidators = true;
});
builder.Services.SwaggerDocument(o =>
{
    o.DocumentSettings = s =>
    {
        s.Title = "Test Api";
        s.Version = "v1";
    };
});
builder.Services.RegisterSomeModuleServices();

builder.Services.AddMassTransit(x =>
{
    x.SetKebabCaseEndpointNameFormatter();
    x.SetInMemorySagaRepositoryProvider();

    var entryAssembly = Assembly.GetEntryAssembly();
    x.AddConsumers(entryAssembly);
    x.AddSagaStateMachines(entryAssembly);
    x.AddSagas(entryAssembly);
    x.AddActivities(entryAssembly);
    
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host("localhost", "/", h =>
        {
            h.Username("user");
            h.Password("user");
        });

        cfg.ConfigureEndpoints(context);
    });
});

builder.Services.AddCors(_ =>
{
    _.AddPolicy("cors", policy =>
    {
        var allowedDomains = new List<string>();
        var exposedHeaders = new List<string>();

        builder.Configuration.GetSection("Cors:AllowedDomains")
            .Bind(allowedDomains);
        builder.Configuration.GetSection("Cors:ExposedHeaders")
            .Bind(exposedHeaders);

        policy.WithOrigins(allowedDomains.ToArray())
            .AllowAnyHeader()
            .WithExposedHeaders(exposedHeaders.ToArray())
            .AllowAnyMethod()
            .AllowCredentials();
    });
});

var app = builder.Build();

using var scope = app.Services.CreateScope();

// var context = scope.ServiceProvider.GetRequiredService<SomeDbContext>();
//
// await context.Database.EnsureDeletedAsync();
// await context.Database.EnsureCreatedAsync();

app.UseCors("cors");

app.UseAuthorization();
app.UseFastEndpoints(c =>
{
    c.Serializer.Options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    c.Endpoints.RoutePrefix = "api";
});
app.UseSwaggerGen();

app.Run();