using Dunet;

namespace TestFastEnd.Common;

[Union]
public partial record Option<T>
{
    partial record Some(T Value);

    partial record None();
}